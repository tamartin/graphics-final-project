#ifndef ROTBEZIER_H
#define ROTBEZIER_H

#include "quat.h"

class rotbezier {
  public:
    Quat cp1;
    Quat cp2;
    Quat cp3;
    Quat cp4;
    //Initialize Variables
    rotbezier(Quat control1, Quat control2, Quat control3, Quat control4);

    //Find the right spot on the curve depending on the clock time
    Quat getRotation(float clock);
};

#endif
