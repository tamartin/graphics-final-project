////////////////////////////////////////////////////////////////////////
//
//   Westmont College
//   CS150 : 3D Computer Graphics
//   Professor David Hunter
//
//   Code derived from book code for _Foundations of 3D Computer Graphics_
//   by Steven Gortler.  See AUTHORS file for more details.
//
////////////////////////////////////////////////////////////////////////

#include <vector>
#include <string>
#include <memory>
#include <stdexcept>
#if __GNUG__
#   include <tr1/memory>
#endif

#include <GL/glew.h>
#ifdef __MAC__
#   include <GLUT/glut.h>
#else
#   include <GL/glut.h>
#endif

#include "cvec.h"
#include "matrix4.h"
#include "geometrymaker.h"
#include "ppm.h"
#include "glsupport.h"
#include "quat.h"
#include "quatrbt.h"
#include "transbezier.h"
#include "rotbezier.h"

using namespace std; // for string, vector, iostream, and other standard C++ stuff
using namespace tr1; // for shared_ptr

// G L O B A L S ///////////////////////////////////////////////////

// --------- IMPORTANT --------------------------------------------------------
// Before you start working on this assignment, set the following variable
// properly to indicate whether you want to use OpenGL 2.x with GLSL 1.0 or
// OpenGL 3.x+ with GLSL 1.3.
//
// Set g_Gl2Compatible = true to use GLSL 1.0 and g_Gl2Compatible = false to
// use GLSL 1.3. Make sure that your machine supports the version of GLSL you
// are using. In particular, on Mac OS X currently there is no way of using
// OpenGL 3.x with GLSL 1.3 when GLUT is used.
//
// If g_Gl2Compatible=true, shaders with -gl2 suffix will be loaded.
// If g_Gl2Compatible=false, shaders with -gl3 suffix will be loaded.
// To complete the assignment you only need to edit the shader files that get
// loaded
// ----------------------------------------------------------------------------
static const bool g_Gl2Compatible = true;

static const float g_frustMinFov = 60.0;  // A minimal of 60 degree field of view
static float g_frustFovY = g_frustMinFov; // FOV in y direction (updated by updateFrustFovY)

static const float g_frustNear = -0.1;    // near plane
static const float g_frustFar = -50.0;    // far plane
static const float g_groundY = -2.0;      // y coordinate of the ground
static const float g_groundSize = 10.0;   // half the ground length

static int g_windowWidth = 700;
static int g_windowHeight = 700;
static bool g_mouseClickDown = false;    // is the mouse button pressed
static bool g_mouseLClickButton, g_mouseRClickButton, g_mouseMClickButton;
static int g_mouseClickX, g_mouseClickY; // coordinates for mouse click event

// Animation globals for time-based animation
static const float g_animStart = 0;
static const float g_animMax = 1; 
static float g_animClock = g_animStart; // clock parameter runs from g_animStart to g_animMax then repeats
static float g_animSpeed = 0.10;         // clock units per second
static int g_elapsedTime = 0;           // keeps track of how long it takes between frames
static float g_animIncrement = g_animSpeed/60.0; // updated by idle() based on GPU speed

struct ShaderState {
  GlProgram program;

  // Handles to uniform variables
  GLint h_uLight;
  GLint h_uProjMatrix;
  GLint h_uModelViewMatrix;
  GLint h_uNormalMatrix;
  GLint h_uColor;
  GLint h_uTexUnit0; 

  // Handles to vertex attributes
  GLint h_aPosition;
  GLint h_aTexCoord0;
  GLint h_aNormal;

  ShaderState(const char* vsfn, const char* fsfn) {
    readAndCompileShader(program, vsfn, fsfn);

    const GLuint h = program; // short hand

    // Retrieve handles to uniform variables
    h_uLight = safe_glGetUniformLocation(h, "uLight");
    h_uProjMatrix = safe_glGetUniformLocation(h, "uProjMatrix");
    h_uModelViewMatrix = safe_glGetUniformLocation(h, "uModelViewMatrix");
    h_uNormalMatrix = safe_glGetUniformLocation(h, "uNormalMatrix");
    h_uColor = safe_glGetUniformLocation(h, "uColor");
    h_uTexUnit0 = safe_glGetUniformLocation(h, "uTexUnit0"); 

    // Retrieve handles to vertex attributes
    h_aPosition = safe_glGetAttribLocation(h, "aPosition");
    h_aNormal = safe_glGetAttribLocation(h, "aNormal");
    h_aTexCoord0 = safe_glGetAttribLocation(h, "aTexCoord0"); 

    if (!g_Gl2Compatible)
      glBindFragDataLocation(h, 0, "fragColor");
    checkGlErrors();
  }

};

static const char * const g_shaderFiles[2] = 
  {"./shaders/basic-gl3.vshader", "./shaders/textured-gl3.fshader"};
static const char * const g_shaderFilesGl2[2] = 
  {"./shaders/basic-gl2.vshader", "./shaders/textured-gl2.fshader"};

static shared_ptr<ShaderState>  g_shaderState; // our global shader state

static shared_ptr<GlTexture> g_tex0, g_tex1, g_tex2, g_tex3, g_tex4, g_tex5, g_tex6; // our global texture instance

// --------- Geometry

// Macro used to obtain relative offset of a field within a struct
#define FIELD_OFFSET(StructType, field) &(((StructType *)0)->field)

// A vertex with floating point Position, Normal, and one set of teXture coordinates;
// We won't use normals for Project #2, but we will need them later.
struct VertexPNX {
  Cvec3f p, n; // position and normal vectors
  Cvec2f x; // texture coordinates

  VertexPNX() {}

  VertexPNX(float x, float y, float z,
            float nx, float ny, float nz,
            float u, float v)
    : p(x,y,z), n(nx, ny, nz), x(u, v) 
  {}

  VertexPNX(const Cvec3f& pos, const Cvec3f& normal, const Cvec2f& texCoords)
    :  p(pos), n(normal), x(texCoords) {}

  VertexPNX(const Cvec3& pos, const Cvec3& normal, const Cvec2& texCoords)
    : p(pos[0], pos[1], pos[2]), n(normal[0], normal[1], normal[2]), x(texCoords[0], texCoords[1]) {}

  // Define copy constructor and assignment operator from GenericVertex so we can
  // use make* functions from geometrymaker.h
  VertexPNX(const GenericVertex& v) {
    *this = v;
  }

  VertexPNX& operator = (const GenericVertex& v) {
    p = v.pos;
    n = v.normal;
    x = v.tex;
    return *this;
  }
};

struct Geometry {
  GlBufferObject vbo, ibo;
  int vboLen, iboLen;

  Geometry(VertexPNX *vtx, unsigned short *idx, int vboLen, int iboLen) {
    this->vboLen = vboLen;
    this->iboLen = iboLen;

    // Now create the VBO and IBO
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(VertexPNX) * vboLen, vtx, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned short) * iboLen, idx, GL_STATIC_DRAW);
  }

  void draw(const ShaderState& curSS) {
    // Enable the attributes used by our shader
    safe_glEnableVertexAttribArray(curSS.h_aPosition);
    safe_glEnableVertexAttribArray(curSS.h_aTexCoord0); 

    // bind vbo
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    safe_glVertexAttribPointer(curSS.h_aPosition, 3, GL_FLOAT, GL_FALSE, sizeof(VertexPNX), FIELD_OFFSET(VertexPNX, p));
    safe_glVertexAttribPointer(curSS.h_aTexCoord0, 2, GL_FLOAT, GL_FALSE, sizeof(VertexPNX), FIELD_OFFSET(VertexPNX, x));

    // bind ibo
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);

    // draw!
    glDrawElements(GL_TRIANGLES, iboLen, GL_UNSIGNED_SHORT, 0);

    // Disable the attributes used by our shader
    safe_glDisableVertexAttribArray(curSS.h_aPosition);
    safe_glDisableVertexAttribArray(curSS.h_aTexCoord0); 
  }
};

// Vertex buffer and index buffer associated with the ground and cube geometry
static shared_ptr<Geometry> g_ground, g_sun, g_cube, g_earth, g_jupiter, g_mercury, g_dot, g_tube;

// --------- Scene

static QuatRBT g_skyRbt = QuatRBT(Cvec3(0.0, 6.0, 11.0));
static QuatRBT g_skyRbt2 = QuatRBT(Cvec3(0.0, 15.0, 0.0), Quat::makeXRotation(-90)* Quat::makeZRotation(180));
static QuatRBT sun = QuatRBT(Cvec3(0,6,0));
static QuatRBT earth, jupiter, mercury, ship;
static QuatRBT g_objectRbt[5] = {sun, earth, jupiter, mercury, ship}; 
static Cvec3f g_objectColors = Cvec3f(0.5, 0.5, 0.5);
int vcount = 0;

///////////////// END OF G L O B A L S //////////////////////////////////////////////////

static void initGround() {
  // A x-z plane at y = g_groundY of dimension [-g_groundSize, g_groundSize]^2
  VertexPNX vtx[4] = {
    VertexPNX(-g_groundSize, g_groundY, -g_groundSize, 0, 1, 0, 0, 0),   // The ground is going to
    VertexPNX(-g_groundSize, g_groundY,  g_groundSize, 0, 1, 0, 0, 10),  // be really big, so we are
    VertexPNX( g_groundSize, g_groundY,  g_groundSize, 0, 1, 0, 10, 10), // going to tile the texture
    VertexPNX( g_groundSize, g_groundY, -g_groundSize, 0, 1, 0, 10, 0),  // 10 by 10 instead of stretching
  };
  unsigned short idx[] = {0, 1, 2, 0, 2, 3};
  g_ground.reset(new Geometry(&vtx[0], &idx[0], 4, 6));
}

static void initCubes() {
  int ibLen, vbLen;
  getCubeVbIbLen(vbLen, ibLen);

  // Temporary storage for cube geometry
  vector<VertexPNX> vtx(vbLen);
  vector<unsigned short> idx(ibLen);

  makeCube(0.1, vtx.begin(), idx.begin());
  g_cube.reset(new Geometry(&vtx[0], &idx[0], vbLen, ibLen));
}

static void initSpheres() {
  int ibLen, vbLen;
  getSphereVbIbLen(20, 20, vbLen, ibLen);

  // Temporary storage for sphere geometry
  vector<VertexPNX> vtx(vbLen);
  vector<unsigned short> idx(ibLen);

  makeSphere(1, 20, 20, vtx.begin(), idx.begin());
  g_sun.reset(new Geometry(&vtx[0], &idx[0], vbLen, ibLen));

  makeSphere(0.15, 20, 20, vtx.begin(), idx.begin());
  g_earth.reset(new Geometry(&vtx[0], &idx[0], vbLen, ibLen));

  makeSphere(0.3, 20, 20, vtx.begin(), idx.begin());
  g_jupiter.reset(new Geometry(&vtx[0], &idx[0], vbLen, ibLen));

  makeSphere(0.1, 20, 20, vtx.begin(), idx.begin());
  g_mercury.reset(new Geometry(&vtx[0], &idx[0], vbLen, ibLen));

  makeSphere(0.05, 20, 20, vtx.begin(), idx.begin());
  g_dot.reset(new Geometry(&vtx[0], &idx[0], vbLen, ibLen));
}

static void initTube() {
  int ibLen, vbLen;
  getTubeVbIbLen(20, vbLen, ibLen);

  // Temporary storage for sphere geometry
  vector<VertexPNX> vtx(vbLen);
  vector<unsigned short> idx(ibLen);

  makeTube(0.01, 0.05, 20, vtx.begin(), idx.begin());
  g_tube.reset(new Geometry(&vtx[0], &idx[0], vbLen, ibLen));
}

// takes a projection matrix and send to the the shaders
static void sendProjectionMatrix(const ShaderState& curSS, const Matrix4& projMatrix) {
  GLfloat glmatrix[16];
  projMatrix.writeToColumnMajorMatrix(glmatrix); // send projection matrix
  safe_glUniformMatrix4fv(curSS.h_uProjMatrix, glmatrix);
}

// takes model view matrix to the shaders
static void sendModelViewMatrix(const ShaderState& curSS, const Matrix4& MVM) {
  GLfloat glmatrix[16];
  MVM.writeToColumnMajorMatrix(glmatrix); // send MVM
  safe_glUniformMatrix4fv(curSS.h_uModelViewMatrix, glmatrix);
}

// takes MVM and its normal matrix to the shaders
static void sendModelViewNormalMatrix(const ShaderState& SS, const Matrix4& MVM, const Matrix4& NMVM) {
  GLfloat glmatrix[16];
  MVM.writeToColumnMajorMatrix(glmatrix); // send MVM
  safe_glUniformMatrix4fv(SS.h_uModelViewMatrix, glmatrix);

  NMVM.writeToColumnMajorMatrix(glmatrix); // send NMVM
  safe_glUniformMatrix4fv(SS.h_uNormalMatrix, glmatrix);
}

// update g_frustFovY from g_frustMinFov, g_windowWidth, and g_windowHeight
static void updateFrustFovY() {
  if (g_windowWidth >= g_windowHeight)
    g_frustFovY = g_frustMinFov;
  else {
    const double RAD_PER_DEG = 0.5 * CS150_PI/180;
    g_frustFovY = atan2(sin(g_frustMinFov * RAD_PER_DEG) * g_windowHeight / g_windowWidth, cos(g_frustMinFov * RAD_PER_DEG)) / RAD_PER_DEG;
  }
}

static Matrix4 makeProjectionMatrix() {
  return Matrix4::makeProjection(
           g_frustFovY, g_windowWidth / static_cast <double> (g_windowHeight),
           g_frustNear, g_frustFar);
}

bool animate = false;
Cvec3 lastcamera = g_skyRbt.getTranslation();
int acount = 0;
static void drawStuff() {
  // short hand for current shader state
  const ShaderState& curSS = *g_shaderState;

  // build & send proj. matrix to vshader
  const Matrix4 projmat = makeProjectionMatrix();
  sendProjectionMatrix(curSS, projmat);

  safe_glUniform3f(curSS.h_uLight, sun.getTranslation()[0], sun.getTranslation()[1], sun.getTranslation()[2]);

  g_objectRbt[1] = QuatRBT(Cvec3(4*cos(g_animClock * CS150_PI * 2), 6, 2.5*sin(g_animClock * CS150_PI * 2)), Quat::makeYRotation(6 * g_animClock * 360) * Quat::makeXRotation(90));
  
  g_objectRbt[2] = QuatRBT(Cvec3(2*sin(g_animClock * CS150_PI *2), 5*cos(g_animClock * CS150_PI * 2) + 6, 0), Quat::makeZRotation(4* g_animClock * 360) * Quat::makeXRotation(180));

  g_objectRbt[3] = QuatRBT(Cvec3(0,6,0)) * QuatRBT(Quat::makeZRotation(45)) * QuatRBT(Cvec3(5*sin(g_animClock * CS150_PI * 2), 0, 3*cos(g_animClock * CS150_PI * 2)), Quat::makeYRotation(6 * g_animClock * 360) * Quat::makeXRotation(90));
  
  Cvec3 eloc = g_objectRbt[1].getTranslation();
  Cvec3 jloc = g_objectRbt[2].getTranslation();
  Cvec3 mloc = g_objectRbt[3].getTranslation();
  Quat erot = g_objectRbt[1].getRotation();
  Quat jrot = g_objectRbt[2].getRotation();
  Quat mrot = g_objectRbt[3].getRotation();
  
  if(g_animClock < 0.33){
    Cvec3 direction1 = jloc - mloc;
    direction1 = direction1 * 0.01;
    Cvec3 direction2 = mloc - eloc;
    direction2 = direction2 * 0.01;
    transbezier tb = transbezier(eloc, eloc + direction1, jloc - direction2, jloc); 
    Quat rot1 = jrot - mrot;
    rot1 = rot1 * 0.01;
    Quat rot2 = mrot - erot;
    rot2 = rot2 * 0.01;
    rotbezier rb = rotbezier(erot, erot + rot1, jrot - rot2, jrot);
    g_objectRbt[4] = QuatRBT(tb.getLocation(g_animClock * 3), rb.getRotation(g_animClock * 3));
 }
  else if(g_animClock < 0.66){
    Cvec3 direction1 = mloc - eloc;
    direction1 = direction1 * 0.01;
    Cvec3 direction2 = eloc - jloc;
    direction2 = direction2 * 0.01;
    transbezier tb = transbezier(jloc, jloc + direction1, mloc - direction2, mloc); 
    Quat rot1 = mrot - erot;
    rot1 = rot1 * 0.01;
    Quat rot2 = erot - jrot;
    rot2 = rot2 * 0.01;
    rotbezier rb = rotbezier(jrot, jrot + rot1, mrot - rot2, mrot);
    g_objectRbt[4] = QuatRBT(tb.getLocation(g_animClock * 3 - 1), rb.getRotation(g_animClock * 3 - 1));
 }
  else {
    Cvec3 direction1 = eloc - jloc;
    direction1 = direction1 * 0.01;
    Cvec3 direction2 = jloc - mloc;
    direction2 = direction2 * 0.01;
    transbezier tb = transbezier(mloc, mloc + direction1, eloc - direction2, eloc); 
    Quat rot1 = erot - jrot;
    rot1 = rot1 * 0.01;
    Quat rot2 = jrot - mrot;
    rot2 = rot2 * 0.01;
    rotbezier rb = rotbezier(mrot, mrot + rot1, erot - rot2, erot);
    g_objectRbt[4] = QuatRBT(tb.getLocation(g_animClock * 3 - 2), rb.getRotation(g_animClock * 3 - 2));
 }

  // set current eye position
  QuatRBT eyeRbt;
  Cvec3 earthloc = g_objectRbt[1].getTranslation();
  if(vcount%2 == 1) eyeRbt = g_skyRbt2;
  else if(animate == false && acount%2 == 0) eyeRbt =  QuatRBT(lastcamera, g_skyRbt.getRotation());
  else if(animate == false && acount%2 ==1){
    lastcamera = earthloc + Cvec3(0,0,0.5);
    eyeRbt = QuatRBT(lastcamera, g_skyRbt.getRotation());
  }
  else if(acount%2 == 1){
    Cvec3 desiredcameraloc = earthloc + Cvec3(0,0,0.5);
    lastcamera = earthloc * 0.1 + lastcamera * 0.9;
    eyeRbt = QuatRBT(lastcamera, g_skyRbt.getRotation());
    if(abs(lastcamera[0] - earthloc[0]) < 0.51 && abs(lastcamera[1] - earthloc[1]) < 0.51 && abs(lastcamera[2] - earthloc[2]) < 0.51) animate = false;
  }
  else {
    Cvec3 desiredcameraloc = g_skyRbt.getTranslation();
    lastcamera = desiredcameraloc * 0.25 + lastcamera * 0.75;
    eyeRbt = QuatRBT(lastcamera, g_skyRbt.getRotation());
    if(abs(lastcamera[0] - desiredcameraloc[0]) < 0.01 && abs(lastcamera[1] - desiredcameraloc[1]) < 0.01 && abs(lastcamera[2] - desiredcameraloc[2]) < 0.01) animate = false;
  }
    
  QuatRBT invEyeRbt = inv(eyeRbt);

  Matrix4 MVM = rigTFormToMatrix(invEyeRbt * QuatRBT(Cvec3(0,0,0)));
  Matrix4 NMVM = normalMatrix(MVM);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, 0.1, 0.1, 0.1); // set color
  safe_glUniform1i(curSS.h_uTexUnit0, 5); // texture unit 5 for ground
  g_ground->draw(curSS);

  MVM = rigTFormToMatrix(invEyeRbt * QuatRBT(Cvec3(0,0,-10)));
  NMVM = normalMatrix(MVM);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, 0.1, 0.1, 0.1); // set color
  safe_glUniform1i(curSS.h_uTexUnit0, 5); // texture unit 5 for ground
  g_ground->draw(curSS);

  MVM = rigTFormToMatrix(invEyeRbt * QuatRBT(Cvec3(0,0,-20)));
  NMVM = normalMatrix(MVM);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, 0.1, 0.1, 0.1); // set color
  safe_glUniform1i(curSS.h_uTexUnit0, 5); // texture unit 5 for ground
  g_ground->draw(curSS);

  MVM = rigTFormToMatrix(invEyeRbt * QuatRBT(Cvec3(0,0,-30)));
  NMVM = normalMatrix(MVM);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, 0.1, 0.1, 0.1); // set color
  safe_glUniform1i(curSS.h_uTexUnit0, 5); // texture unit 5 for ground
  g_ground->draw(curSS);

  MVM = rigTFormToMatrix(invEyeRbt * QuatRBT(Cvec3(0,0,-40)));
  NMVM = normalMatrix(MVM);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, 0.1, 0.1, 0.1); // set color
  safe_glUniform1i(curSS.h_uTexUnit0, 5); // texture unit 5 for ground
  g_ground->draw(curSS);

  MVM = rigTFormToMatrix(invEyeRbt * g_objectRbt[0]);
  NMVM = normalMatrix(MVM);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, g_objectColors[0], g_objectColors[1], g_objectColors[2]);
  safe_glUniform1i(curSS.h_uTexUnit0, 0); // texture unit 0 for sun
  g_sun->draw(curSS);

  MVM = rigTFormToMatrix(invEyeRbt * g_objectRbt[1]);
  NMVM = normalMatrix(MVM);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, g_objectColors[0], g_objectColors[1], g_objectColors[2]);
  safe_glUniform1i(curSS.h_uTexUnit0, 1); // texture unit 1 for earth
  g_earth->draw(curSS);
  
  MVM = rigTFormToMatrix(invEyeRbt * g_objectRbt[2]);
  NMVM = normalMatrix(MVM);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, g_objectColors[0], g_objectColors[1], g_objectColors[2]);
  safe_glUniform1i(curSS.h_uTexUnit0, 2); // texture unit 2 for jupiter
  g_jupiter->draw(curSS);

  MVM = rigTFormToMatrix(invEyeRbt * g_objectRbt[3]);
  NMVM = normalMatrix(MVM);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, g_objectColors[0], g_objectColors[1], g_objectColors[2]);
  safe_glUniform1i(curSS.h_uTexUnit0, 3); // texture unit 3 for mercury
  g_mercury->draw(curSS);

  Matrix4 light = rigTFormToMatrix(sun); 
  Matrix4 shadowMatrix(0);
  shadowMatrix(0,0) = shadowMatrix(2,2) = shadowMatrix(3,3) = light(1,3);
  shadowMatrix(0,1) = light(0,3) * -1;
  shadowMatrix(2,1) = light(2,3) * -1;
  shadowMatrix(3,1) = -1;

  MVM = rigTFormToMatrix(invEyeRbt) * shadowMatrix * rigTFormToMatrix(g_objectRbt[2]);
  sendModelViewMatrix(curSS, MVM);
  safe_glUniform3f(curSS.h_uColor, 1.0, 1.0, 1.0);
  safe_glUniform1i(curSS.h_uTexUnit0, 4); //texture unit 4 for shadow 
  g_jupiter->draw(curSS);

  MVM = rigTFormToMatrix(invEyeRbt) * shadowMatrix * rigTFormToMatrix(g_objectRbt[3]);
  sendModelViewMatrix(curSS, MVM);
  safe_glUniform3f(curSS.h_uColor, 1.0, 1.0, 1.0);
  safe_glUniform1i(curSS.h_uTexUnit0, 4); //texture unit 4 for shadow 
  g_mercury->draw(curSS);

  MVM = rigTFormToMatrix(invEyeRbt * g_objectRbt[4]);
  NMVM = normalMatrix(MVM);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, g_objectColors[0], g_objectColors[1], g_objectColors[2]);
  safe_glUniform1i(curSS.h_uTexUnit0, 6); // texture unit 6 for ship
  g_dot->draw(curSS);

  MVM = rigTFormToMatrix(invEyeRbt * g_objectRbt[4] * QuatRBT(Cvec3(0,0,0.075)));
  NMVM = normalMatrix(MVM);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, g_objectColors[0], g_objectColors[1], g_objectColors[2]);
  safe_glUniform1i(curSS.h_uTexUnit0, 6); // texture unit 6 for ship
  g_tube->draw(curSS);

  MVM = rigTFormToMatrix(invEyeRbt * g_objectRbt[4] * QuatRBT(Cvec3(0,0,0.15)));
  NMVM = normalMatrix(MVM);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, g_objectColors[0], g_objectColors[1], g_objectColors[2]);
  safe_glUniform1i(curSS.h_uTexUnit0, 6); // texture unit 6 for ship
  g_cube->draw(curSS);

  MVM = rigTFormToMatrix(invEyeRbt * g_objectRbt[4] * QuatRBT(Cvec3(0,0,0.225)));
  NMVM = normalMatrix(MVM);
  sendModelViewNormalMatrix(curSS, MVM, NMVM);
  safe_glUniform3f(curSS.h_uColor, g_objectColors[0], g_objectColors[1], g_objectColors[2]);
  safe_glUniform1i(curSS.h_uTexUnit0, 6); // texture unit 6 for ship
  g_tube->draw(curSS);
}

static void display() {
  glUseProgram(g_shaderState->program);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);                   // clear framebuffer color&depth

  drawStuff();

  glutSwapBuffers();                                    // show the back buffer (where we rendered stuff)

  checkGlErrors();

  // Calculate frames per second 
  static int oldTime = -1;
  static int frames = 0;
  static int lastTime = -1;

  int currentTime = glutGet(GLUT_ELAPSED_TIME); // returns milliseconds
  g_elapsedTime = currentTime - lastTime;       // how long last frame took
  lastTime = currentTime;

        if (oldTime < 0)
                oldTime = currentTime;

        frames++;

        if (currentTime - oldTime >= 5000) // report FPS every 5 seconds
        {
                cout << "Frames per second: "
                        << float(frames)*1000.0/(currentTime - oldTime) << endl;
                cout << "Elapsed ms since last frame: " << g_elapsedTime << endl;
                oldTime = currentTime;
                frames = 0;
        }
}

static void reshape(const int w, const int h) {
  g_windowWidth = w;
  g_windowHeight = h;
  glViewport(0, 0, w, h);
  cerr << "Size of window is now " << w << "x" << h << endl;
  updateFrustFovY();
  glutPostRedisplay();
}

int ocount = 0;
int mcount = 0;
static void motion(const int x, const int y) {
  const double dx = x - g_mouseClickX;
  const double dy = g_windowHeight - y - 1 - g_mouseClickY;

  QuatRBT m;
  if (g_mouseLClickButton && !g_mouseRClickButton) { // left button down?
    m = QuatRBT(Quat::makeXRotation(-dy)) * QuatRBT(Quat::makeYRotation(dx));
  }
  else if (g_mouseRClickButton && !g_mouseLClickButton) { // right button down?
    m = QuatRBT(Cvec3(dx, dy, 0) * 0.01);
  }
  else if (g_mouseMClickButton || (g_mouseLClickButton && g_mouseRClickButton)) {  // middle or (left and right) button down?
    m = QuatRBT(Cvec3(0, 0, -dy) * 0.01);
  }

  if (g_mouseClickDown) {
    QuatRBT a;
    QuatRBT eyeRbt;
    if(vcount%3 == 0) eyeRbt =  g_objectRbt[2];
    else if(vcount%3 == 1) eyeRbt = g_objectRbt[0];
    else eyeRbt = g_objectRbt[1];
    a = transFact(g_objectRbt[0]) * linFact(eyeRbt);
    g_objectRbt[0] = a * m * inv(a) * g_objectRbt[0];
    glutPostRedisplay(); // we always redraw if we changed the scene
  }

  g_mouseClickX = x;
  g_mouseClickY = g_windowHeight - y - 1;
}


static void mouse(const int button, const int state, const int x, const int y) {
  g_mouseClickX = x;
  g_mouseClickY = g_windowHeight - y - 1;  // conversion from GLUT window-coordinate-system to OpenGL window-coordinate-system

  g_mouseLClickButton |= (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN);
  g_mouseRClickButton |= (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN);
  g_mouseMClickButton |= (button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN);

  g_mouseLClickButton &= !(button == GLUT_LEFT_BUTTON && state == GLUT_UP);
  g_mouseRClickButton &= !(button == GLUT_RIGHT_BUTTON && state == GLUT_UP);
  g_mouseMClickButton &= !(button == GLUT_MIDDLE_BUTTON && state == GLUT_UP);

  g_mouseClickDown = g_mouseLClickButton || g_mouseRClickButton || g_mouseMClickButton;
}

static void idle()
{
  g_animIncrement = g_animSpeed * g_elapsedTime / 1000; // rescale animation increment
  g_animClock += g_animIncrement;           // Update animation clock 
         if (g_animClock > g_animMax)       // and cycle to start if necessary.
		 g_animClock = g_animStart;
         glutPostRedisplay();  // for animation
}

static void keyboard(const unsigned char key, const int x, const int y) {
  switch (key) {
  case 27:
    exit(0); //esc
  case 'a':
    if(animate == false && vcount%2 == 0){
      animate = true;
      acount++;
    }
      break;
  case 'v':
    if(acount%2 == 0 && animate == false) vcount++;  
    break;
  case 'o':
    ocount++;
    break;
  case 'm':
    mcount++;
    break;
  case 'h':
    cout << " ============== H E L P ==============\n\n"
    << "h\t\thelp menu\n"
    << "s\t\tsave screenshot\n"
    << "a\t\tStart the Ease in camera\n"
    << "v\t\tCycle view\n" 
    << endl;
    break;
  case 's':
    glFlush();
    writePpmScreenshot(g_windowWidth, g_windowHeight, "out.ppm");
    cout << "Screenshot written to out.ppm." << endl;
    break;
  }
  glutPostRedisplay();
}

static void initGlutState(int argc, char * argv[]) {
  glutInit(&argc, argv);                                  // initialize Glut based on cmd-line args
  glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE|GLUT_DEPTH);  //  RGBA pixel channels and double buffering
  glutInitWindowSize(g_windowWidth, g_windowHeight);      // create a window
  glutCreateWindow("CS-150: Final Project");                  // title the window

  glutDisplayFunc(display);                               // display rendering callback
  glutReshapeFunc(reshape);                               // window reshape callback
  glutMotionFunc(motion);                                 // mouse movement callback
  glutMouseFunc(mouse);                                   // mouse click callback
  glutIdleFunc(idle);
  glutKeyboardFunc(keyboard);
}

static void initGLState() {
  glClearColor(128./255., 200./255., 255./255., 0.);
  glClearDepth(0.);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glPixelStorei(GL_PACK_ALIGNMENT, 1);
  glCullFace(GL_BACK);
  glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_GREATER);
  glReadBuffer(GL_BACK);
  if (!g_Gl2Compatible)
    glEnable(GL_FRAMEBUFFER_SRGB);
}

static void initShaders() {
    if (g_Gl2Compatible)
      g_shaderState.reset(new ShaderState(g_shaderFilesGl2[0], g_shaderFilesGl2[1]));
    else
      g_shaderState.reset(new ShaderState(g_shaderFiles[0], g_shaderFiles[1]));
}

static void initGeometry() {
  initCubes();
  initGround();
  initSpheres();
  initTube();
}

static void loadTexture(GLuint texHandle, const char *ppmFilename) {
  int texWidth, texHeight;
  vector<PackedPixel> pixData;

  ppmRead(ppmFilename, texWidth, texHeight, pixData);

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, texHandle);
  glTexImage2D(GL_TEXTURE_2D, 0, g_Gl2Compatible ? GL_RGB : GL_SRGB, texWidth, texHeight,
               0, GL_RGB, GL_UNSIGNED_BYTE, &pixData[0]);
  checkGlErrors();
}

static void initTextures() {
  g_tex0.reset(new GlTexture());
  g_tex1.reset(new GlTexture());
  g_tex2.reset(new GlTexture());
  g_tex3.reset(new GlTexture());
  g_tex4.reset(new GlTexture());
  g_tex5.reset(new GlTexture());
  g_tex6.reset(new GlTexture());

  loadTexture(*g_tex0, "sun.ppm");
  loadTexture(*g_tex1, "earthinverted.ppm");
  loadTexture(*g_tex2, "jupiterinverted.ppm");
  loadTexture(*g_tex3, "mercury.ppm");
  loadTexture(*g_tex4, "shadow.ppm");
  loadTexture(*g_tex5, "ground.ppm");
  loadTexture(*g_tex6, "ship.ppm");
  
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, *g_tex0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, *g_tex1);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

  glActiveTexture(GL_TEXTURE2);
  glBindTexture(GL_TEXTURE_2D, *g_tex2);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

  glActiveTexture(GL_TEXTURE3);
  glBindTexture(GL_TEXTURE_2D, *g_tex3);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

  glActiveTexture(GL_TEXTURE4);
  glBindTexture(GL_TEXTURE_2D, *g_tex4);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

  glActiveTexture(GL_TEXTURE5);
  glBindTexture(GL_TEXTURE_2D, *g_tex5);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

  glActiveTexture(GL_TEXTURE6);
  glBindTexture(GL_TEXTURE_2D, *g_tex6);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
}

int main(int argc, char * argv[]) {
  try {
    initGlutState(argc,argv);

    glewInit(); // load the OpenGL extensions

    cout << (g_Gl2Compatible ? "Will use OpenGL 2.x / GLSL 1.0" : "Will use OpenGL 3.x / GLSL 1.3") << endl;
    if ((!g_Gl2Compatible) && !GLEW_VERSION_3_0)
      throw runtime_error("Error: card/driver does not support OpenGL Shading Language v1.3");
    else if (g_Gl2Compatible && !GLEW_VERSION_2_0)
      throw runtime_error("Error: card/driver does not support OpenGL Shading Language v1.0");

    initGLState();
    initShaders();
    initGeometry();
    initTextures();

    glutMainLoop();
    return 0;
  }
  catch (const runtime_error& e) {
    cout << "Exception caught: " << e.what() << endl;
    return -1;
  }
}
