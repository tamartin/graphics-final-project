#include <iostream>
#include "transbezier.h"
using namespace std;

transbezier::transbezier(Cvec3 control1, Cvec3 control2, Cvec3 control3, Cvec3 control4)
{
  this->cp1 = control1;
  this->cp2 = control2;
  this->cp3 = control3;
  this->cp4 = control4;
}

Cvec3 transbezier::getLocation(float clock){
  Cvec3 loc = cp1 * pow(1-clock,3) + cp2 * 3 * clock * pow(1-clock,2) + cp3 * 3 * pow(clock,2) * (1-clock) + cp4 * pow(clock,3);
  return loc;
}
