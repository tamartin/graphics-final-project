#ifndef TRANSBEZIER_H
#define TRANSBEZIER_H

#include "cvec.h"

class transbezier {
  public:
    Cvec3 cp1;
    Cvec3 cp2;
    Cvec3 cp3;
    Cvec3 cp4;
    //Initialize Variables
    transbezier(Cvec3 control1, Cvec3 control2, Cvec3 control3, Cvec3 control4);

    //Find the right spot on the curve depending on the clock time
    Cvec3 getLocation(float clock);
};

#endif
