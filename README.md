# CS-150, Final Project
## [Taylor Martin]

---

## Context
[I am doing a solar system simulation with a few planets rotating around a star.  There will also be a space ship that flies from planet to planet.]

## Required components
1. **Transformations represented by QuatRBT.** [Star, planets, spaceship.]
2. **Keyframing.** [The spaceship will be moving using the planets as its keyframes.]
3. **Ease-in camera.** [I plan to make the ease-in camera follow the spaceship.]

## Other Features
1. Multiple shaders, including phong lighting and textures.
2. Homemade shadows using projection matrices.
3. Animation of position using an animation increment, as in Project #4.
4. Animation of position using a parametric function of the animation clock, as in Project #6.
5. Interactivity using the mouse and/or keyboard.
6. Tubes, octahedrons, or some other geometry object that you make yourself.
7. Multiple points of view.
8. A help menu that explains all the functionality.

[As of right now I plan on doing all of these "Other Features".]
