#include <iostream>
#include "rotbezier.h"
using namespace std;

rotbezier::rotbezier(Quat control1, Quat control2, Quat control3, Quat control4)
{
  this->cp1 = control1;
  this->cp2 = control2;
  this->cp3 = control3;
  this->cp4 = control4;
}

Quat rotbezier::getRotation(float clock){
  Quat rot = cp1 * pow(1-clock,3) + cp2 * 3 * clock * pow(1-clock,2) + cp3 * 3 * pow(clock,2) * (1-clock) + cp4 * pow(clock,3);
  return rot;
}
